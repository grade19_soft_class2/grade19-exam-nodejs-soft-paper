'use strict';

let nunjunck=require('nunjucks');


function createEnv(path,opts){
    path=path||'views';
    opts=opts||{};
    let optEnv={
        watch:opts.watch===undefined?false:opts.watch,
        noCache:opts.noCache===undefined?false:opts.noCache,
        thowOnUndefined:opts.thowOnUndefined===undefined?true:opts.thowOnUndefinedk,
        autoescape:opts.autoescape===undefined?true:opts.autoescape
    }
    let env=nunjunck.configure(path,optEnv);
    return env;
}

module.exports=async (ctx,next)=>{
    ctx.render=function(view,modle){
        let env=createEnv();
        ctx.body=env.render(view,modle);
    }
    await next();
}