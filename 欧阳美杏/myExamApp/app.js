'use strict';

let Koa = require("koa");
let bodyParser = require('koa-bodyparser');
let statics = require('koa-static');
let controller = require('./controllers');
let templating = require("./templating");

let app = new Koa();

app.use(statics(__dirname+'/static'));

app.use(bodyParser());
app.use(templating);
app.use(controller());





let port = 8000;
app.listen(port);

console.log(`http:127.0.0.1:${port}`);