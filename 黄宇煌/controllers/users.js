'use strict';

// 首页
let home_fn = async (ctx, next) => {
    ctx.render("index.html")
}

// 登录页
let login_fn = async(ctx,next)=>{
    ctx.render("login.html");
}

// 登录操作
let loginDone_fn = async (ctx, next) => {
    let uid = ctx.request.body.username || "";
    let pwd = ctx.request.body.password || "";

    if (uid === "sa" && pwd === "123"){
        ctx.render("index.html")
    }else{
        ctx.body="你的账号密码有误，请重试！！！"
    }
        
}



// 暴露模块
module.exports = {
    "/": ["get", home_fn],
    "/login": ["get", login_fn],
    "/loginDone": ["post", loginDone_fn]
}