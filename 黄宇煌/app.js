'use strict';

// 导入模块
let Koa = require("koa");
let bodyparser = require("koa-bodyparser");
let server = require("koa-static");
let index = require("./controllers");
let templating = require("./templating");

// 实例化Koa模块
let app = new Koa();


// 引用中间件
app.use(templating);
app.use(bodyparser());
app.use(server(__dirname));
app.use(index());

// 监听端口
app.listen(3080,()=>{
    console.log("http://localhost:3080");
});