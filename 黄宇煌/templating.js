'use strict';

// 导入模块
let nunjucks = require("nunjucks");

function createEnv(path, opts) {
    path = path || "";
    opts = opts || {};

    // 环境配置
    let envOptions = {
        // 自动转义(默认true)
        autosescapes: opts.autosescapes === undefined ? true : opts.autosescapes,
        // 不缓存(默认true)
        noCache: opts.noCache === undefined ? true : opts.noCache,
        // 重载（开发模式下，默认ture）
        watch: opts.watch === undefined ? true : opts.watch,
        // 抛出错误（开发模式下，默认true）
        throwOnUndefined: opts.throwOnUndefined === undefined ? true : opts.throwOnUndefined
    }

    let env = nunjucks.configure(path, envOptions);
    return env;
}

module.exports = async(ctx,next)=>{
    // 构建render方法
    ctx.render = function(view,modle){
        let env = createEnv("views",{autosescapes:false})
        ctx.body = env.render(view,modle);
    }

    // 完成后继续执行
    await next();
}