'use strict';

// 1. 导入模块

// 导入koa2框架
const Koa = require('koa');
// 导入静态资源处理的模块
const statics = require('koa-static');
// 导入自行封装的模板引擎中间件
const templating = require('./templating');
// 导入controllers文件夹下的index.js作为集中处理URL的中间件
const router = require('./controllers');
// 导入处理post请求的模块
const bodyParser = require('koa-bodyparser');

// 2. 实例化对象server，相当于一台Web服务器
const server = new Koa();

// 3. 注册使用模块

// 注册使用静态资源处理模块
server.use(statics(__dirname + '/static'));
// 注册使用处理post请求的模块
server.use(bodyParser());
// 注册使用模板引擎中间件
server.use(templating);
// 注册使用处理URL的路由中间件
server.use(router());

// 4. 服务器监听端口
let port = 3000;
server.listen(port, () => {
    console.log(`http://127.0.0.1:${port}`);
});