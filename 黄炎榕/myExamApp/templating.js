'use strict';

// 1. 导入模板引擎模块
const nunjucks = require('nunjucks');

// 2. 创建模板引擎的 Environment 实例
function createEnv(path, opts) {
    // 参数 path 为指定存放模板的目录，如果有指定就用指定的，没有就默认为 views 目录
    path = path || 'views'
    // 参数 opts 为可选功能的开启或关闭，可传值进行更改，默认为空对象
    opts = opts || {}
    // 3. 设置环境的配置选项
    let envOptions = {
        // autoescape：自动转义,输出是否需要自动转义（默认为true）
        autoescape: opts.autoescape === undefined ? true : opts.autoescape,
        // watch：当模板改变时，是否重新加载（默认为false）开发模式下使用true，生产模式下使用false
        watch: opts.watch === undefined ? false : opts.watch,
        // noCache：没有缓存，是否需要缓存（默认为false）开发模式下使用true，生产模式下使用false
        noCache: opts.noCache === undefined ? false : opts.noCache,
        // throwOnUndefined：抛出异常，当输出 null 或 undefined 抛出异常（默认为false）
        throwOnUndefined: opts.throwOnUndefined === undefined ? false : opts.throwOnundefined,
        // trimBlocks：自动去除 block/tag 后面的换行符（默认为false）
        trimBlocks: opts.trimBlocks === undefined ? false : opts.trimBlocks,
        // lstripBlocks：自动去除 block/tag 签名的空格（默认为false）
        lstripBlocks: opts.lstripBlocks === undefined ? false : opts.lstripBlocks
    }
    // 4. 往nunjucks模板引擎里配置
    let env = nunjucks.configure(path, envOptions);
    // 返回 env
    return env;
}

// 5. 此模块作为一个中间件暴露出去，中间件必须为一个函数
module.exports = async (ctx, next) => {
    // 为 ctx 绑定一个 render 方法,方法有两个参数，view 为视图，也就是需要渲染的模板，model 为模型，也就是所需的数据
    ctx.render = function (view, model) {
        // 调用 createEnv() 函数
        let env = createEnv('views', { watch: true, noCache: true });
        // 渲染网页
        ctx.response.body = env.render(view, model);
    }
    // 调用下一个异步函数
    await next();
}