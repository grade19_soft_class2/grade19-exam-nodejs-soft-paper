'use strict';

// 1. 导入模块
const fs = require('fs');
const path = require('path');
const router = require('koa-router')();

// 2. 获取controllers文件夹下所有文件，并且筛选出以 .js 结尾，且不是 index.js 的文件
function searchControllers() {
    // 获取controllers文件夹的绝对路径，并且搜索文件夹下所有文件
    let files = fs.readdirSync(__dirname);
    // 利用filter过滤器循环遍历 files 筛选出以 .js 格式结尾，并且不是 index.js 的文件
    let filterFile = files.filter(fileName => {
        return fileName.endsWith('.js') && fileName !== 'index.js';
    });
    // 将过滤完的结果返回
    return filterFile;
}

// 3. 注册路由
function registerRouter(file) {
    // 遍历 file 将每一个 js 文件都存在 item 中
    file.forEach(item => {
        // 将controllers文件夹的路径与每个 js 文件进行拼接
        let fullPath = path.join(__dirname, item);
        // 导入 fullPath路径，获取里面的路由对象
        let routerObj = require(fullPath);
        // 循环遍历routerObj对象,注册里面的路由
        for (let item in routerObj) {
            // 获取路由请求地址所对应的请求类型
            let type = routerObj[item][0];
            // 获取路由请求地址所对应的处理函数
            let fn = routerObj[item][1];
            // 判断请求类型
            if (type === 'get') {
                // get 请求，参数第一项 item 为请求地址，第二项 fn 是请求地址所对应的处理函数
                router.get(item, fn);
            } else if (type === 'post') {
                // post 请求，同上
                router.post(item, fn);
            } else if (type === 'put') {
                // put 请求，同上
                router.put(item, fn);
            } else if (type === 'delete') {
                // delete 请求，同上
                router.delete(item, fn);
            } else {
                // 请求类型错误
                console.log(`请求类型有误！`);
            }
        }
    });
}

// 4. 此文件作为中间件暴露出去，中间件必须是一个函数
module.exports = function () {
    // 调用函数searchControllers()，获取过滤完的js文件
    let file = searchControllers();
    // 调用函数registerRouter()，并传入过滤完的js文件注册路由
    registerRouter(file);
    // 将路由规则返回出去
    return router.routes();
}