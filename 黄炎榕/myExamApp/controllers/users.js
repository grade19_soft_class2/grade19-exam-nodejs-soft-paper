'use strict';

// 1. 渲染模板并进行传值
let hello_fn = async (ctx, next) => {
    ctx.render('hello.html', { title: "我叫标题", msg: "我叫内容" });
}

// 2. 暴露出去
module.exports = {
    '/': ['get', hello_fn]
}