'use strict'

let koa = require('koa');
let koa_bodyparser = require('koa-bodyparser');
let template = require('./template')
let controllers = require('./controllers/index')
let static=require('./static');

let app = new koa();
app.use(koa_bodyparser())
app.use(controllers())
app.use(template)

app.use(static(__dirname+'/static'));

let port = 3999
app.listen(port)

console.log(`http://127.0.0.1:${port}`);