'use strict'

var fn_login = async(ctx,next)=>{
    ctx.response.body=`
    <form action="/loginPD" method="POST">
    <input type="text" name="userName" id="">
    <input type="password" name="password" id="">
    <input type="submit" name="" value="提交" id="">
</form>`
};

var fn_loginPD = async(ctx,next)=>{
    var userName = ctx.request.body.userName || '';
    var password = ctx.request.body.password || '';

    if(userName === 'fish' && password =='123'){
        ctx.redirect('/loginOk');
    }else{
        ctx.redirect('/loginError')
    }
}
var fn_loginOk = async(ctx,next)=>{
    ctx.response.body=`登录成功`;
};

var fn_loginError = async(ctx,next)=>{
    ctx.response.body='登录失败 账号是fish 密码是123'
}

module.exports={
    '/':['get',fn_login],
    '/loginPD':['post',fn_loginPD],
    '/loginOk':['get',fn_loginOk],
    '/loginError':['get',fn_loginError]
}