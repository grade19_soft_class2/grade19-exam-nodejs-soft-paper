'use strict'

let fs = require('fs');
let path = require('path');

var fn_routerFile = (koa_router, filterFiles) => {
    filterFiles.forEach((item) => {

        var fullFileDir = path.join(__dirname, item);
        var conRouter = require(fullFileDir);

        for (var fn in conRouter) {
            var type = conRouter[fn][0];
            var fn_router = conRouter[fn][1];

            if (type === 'get') {
                koa_router.get(fn, fn_router);
            } else if (type === 'post') {
                koa_router.post(fn, fn_router)
            }
        }
    })
}

var fn_filterFile = (koa_router) => {
    var currentDirFile = fs.readdirSync(__dirname);

    var currentFile = path.basename(__filename);

    var filterFiles = currentDirFile.filter((filterFiles) => {
        return filterFiles.endsWith('js') && filterFiles !== currentFile;
    })

    fn_routerFile(koa_router, filterFiles)
}
module.exports = function(){
    const koa_router = require('koa-router')();
    fn_filterFile(koa_router);
    return koa_router.routes();
}