"use strict"
let nunjucks = require('nunjucks');

    var createEnv = function(view,opts){
    view = view || 'views';
    opts = opts || {};

    var optsOption={
        autoescape: opts.autoescape === undefined ? true : opts.autoescape,
        throwOnUndefined: opts.throwOnUndefined === undefined ? false : opts.throwOnUndefined,
        trimBlocks: opts.trimBlocks === undefined ? false : opts.trimBlocks,
        lstripBlocks: opts.lstripBlocks === undefined ? false : opts.lstripBlocks,
        watch: opts.watch === undefined ? false : opts.watch,
        noCache: opts.noCache === undefined ? false : opts.noCache
    };

    var env = nunjucks.configure(view,optsOption);
    return env
};

module.exports = async(ctx,next)=>{
    ctx.render = function(views,modle){
        var env = createEnv('views');
        return ctx.response.body = env.render(views,modle)
    }
    await next();
}