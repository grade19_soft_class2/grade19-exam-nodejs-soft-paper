'use strict'

let Koa = require('koa');
let controllers = require('./controllers');
let templatings = require('./templating');
let server = require('koa-static');
let app = new Koa();


app.use(templatings);
app.use(controllers());
app.use(server(__dirname));

let port = 7204;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);