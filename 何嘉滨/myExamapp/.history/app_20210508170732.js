'use strict';
let
    Koa = require('koa'),
    templating = require('./templating'),
    staticRes = require('koa-static'),
    bodyParser = require('koa-bodyparser'),
    controller = require('./controllers'),
    model = require('./models');


let app = new Koa();

app.use(bodyParser());
app.use(templating);
app.use(staticRes(__dirname + "/static/"))

app.use(async(ctx, next) => {
    console.log('正在初始化数据库...');
    await model.sync();
    await next();
})
app.use(controller());

let port = 5050;
app.listen(port, () => {
    console.log(`http://localhost:${port}`);
});