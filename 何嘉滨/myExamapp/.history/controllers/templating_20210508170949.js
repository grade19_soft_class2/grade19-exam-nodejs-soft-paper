'use strict';
// 1.引入模块
let
    router = require('koa-router')(),
    fs = require('fs'),
    path = require('path');



function searchRouter(dir) {
    let
        files = fs.readdirSync(dir),
        routerFile = files.filter(file => {
            return file.endsWith('.js') && file !== 'index.js'
        });
    return routerFile
}

function registerRouter(files, dir) {
    files.forEach(item => {
        // 需要引入所有路由文件
        let tmpFile = path.join(dir, item);
        let tmpRouter = require(tmpFile);
        for (let i in tmpRouter) {
            let
                type = tmpRouter[i][0],
                fn = tmpRouter[i][1];
            if (type === 'get') {
                router.get(i, fn)
            } else if (type === 'post') {
                router.post(i, fn)
            }
        }
    });
}


module.exports = function() {
    let dir = __dirname;
    let files = searchRouter(dir);
    registerRouter(files, dir);
    return router.routes();
};