'use strict';
// 1.引入模块
let nunjucks = require('nunjucks');


function creatEnv(path, options) {
    path = path || "views"
    options = options || {}
    let
        envOptions = {
            "watch": options.watch === undefined ? true : options.watch,
            "autoespace": options.autoespace === undefined ? true : options.autoespace,
            "throwOnUndenfined": options.throwOnUndenfined ? true : options.throwOnUndenfined,
            "noCache": options.noCache === undefined ? true : options.noCache
        },
        env = nunjucks.configure(path, envOptions);

    return env
};


module.exports = async(ctx, next) => {
    ctx.render = function(view, model) {
        let env = creatEnv();
        ctx.body = env.render(view, model);
    }
    await next();
};