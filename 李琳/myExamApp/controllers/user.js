'use strict';

let login_fn=(async(ctx,next)=>{
    ctx.redirect('login')
});

let login_done=(async(ctx,next)=>{
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';

    if(username==='Sakura' && password==='123'){
        ctx.redirect('success')
    }else{
        ctx.redirect('fail')
    }
    console.log(username);
    console.log(password);
});

let login_success=(async(ctx,next)=>{
    console.log('登陆成功');
});
let login_fail=(async(ctx,next)=>{
    console.log('登录失败');
});


module.exports=(async(ctx,next)=>{
    '/'=['get',login_fn];
    '/login'=['post',login_done];
    '/success'=['get',login_success];
    'fail'=['get',login_fail];
})