'use strict';
let Koa = require('koa');
let router = require('koa-router');
let bodyParser = require('koa-bodyparser');

let controllers = require('./controllers');
let templating = require('./templating');
/* let static = require('./static'); */

let app = new Koa();
app.use(bodyParser());
app.use(controllers());
/* app.use(static(__dirname)); */
app.use(templating);

app.use(async(ctx,next)=>{
    ctx.render('hello.index')
})

let port = 3000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);