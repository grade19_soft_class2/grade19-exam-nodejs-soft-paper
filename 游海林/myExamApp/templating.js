'use strict'


var nunjucks = require('nunjucks');


function createEnv(path, option) {
     path = path || 'views';
     option = option || {};
    let optionlod = {
        autoescape: option.autoescape === undefined ? true : option.autoescape,
        throwOnUndefined: option.throwOnUndefined === undefined ? true : option.throwOnUndefined,
        trimBlocks: option.trimBlocks === undefined ? true : option.trimBlocks,
        lstripBlocks: option.lstripBlocks === undefined ? true : option.lstripBlocks,
        watch: option.watch === undefined ? true : option.watch,
        noCache: option.noCache === undefined ? true : option.noCache
    }


    let env = nunjucks.configure(path, optionlod)
    return env;
}


module.exports = async (ctx, next) => {

    ctx.render = (view, module) => {
        let env = createEnv()
        ctx.body = env.render(view, module)
    }
    await next();
}