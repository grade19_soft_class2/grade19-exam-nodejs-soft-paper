'use strict'


let index_fn = async (ctx, next) => {
   ctx.render('login.html')
}


let login_fn = async (ctx,next)=>{
let username = ctx.request.body.username||'';
let password = ctx.request.body.password||'';

if(username==='admin'&& password==='123456'){
   ctx.redirect('/login/success')

}else{

    ctx.redirect('/login/error')
}


}


let login_success_fn = async (ctx, next) => {
    ctx.render('index.html')
}

let login_error_fn = async (ctx, next) => {
    ctx.response.body = '<h1>账号或密码错误</h1>';
}
module.exports = {
    '/': ['get', index_fn],
    '/login/login': ['post', login_fn],
    '/login/success': ['get', login_success_fn],
    '/login/error': ['get', login_error_fn]
}