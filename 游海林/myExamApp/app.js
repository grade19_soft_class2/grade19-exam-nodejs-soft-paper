'use strict'

const Koa = require('koa');

const bodyParser = require('koa-bodyparser')
var templating = require('./templating')
var st = require('koa-static');
const controller = require('./controllers');
const app = new Koa();
const path = require('path')
app.use(templating)
app.use(bodyParser());

app.use(controller())



app.use(st(
    path.join( __dirname)
))



app.use(async (ctx,next)=>{
    ctx.render('layout.html')

})


const port = 3000;
app.listen(port);

console.log(`http://127.0.0.1:${port}`);