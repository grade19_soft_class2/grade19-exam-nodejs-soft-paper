'use strict'

var fn_userAdd = async (ctx, next) => {
    ctx.response.body = `<h1 style="text-align: center;">用户添加页面</h1>`;
};
var fn_userDelete = async (ctx, next) => {
    ctx.response.body = `<h1 style="text-align: center;">用户删除页面</h1>`;
};
var fn_userUpdate = async (ctx, next) => {
    ctx.response.body = `<h1 style="text-align: center;">用户修改页面</h1>`;
};
var fn_userSelect = async (ctx, next) => {
    ctx.response.body = `<h1 style="text-align: center;">用户查询页面</h1>`;
};


module.exports = {
    '/userAdd':['get',fn_userAdd],
    '/userDelete':['get',fn_userDelete],
    '/userUpdate':['get',fn_userUpdate],
    '/userSelect':['get',fn_userSelect],
}