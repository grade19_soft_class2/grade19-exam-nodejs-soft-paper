'use strict'

const fs = require('fs');
const path = require('path');
const router = require('koa-router')();

var fn_filterFile = function(){
    var currentDirFile = fs.readdirSync(__dirname);
    var currentFile = path.basename(__filename);
    
    var filterFile = currentDirFile.filter(file=>{
        return file.endsWith('.js') && file !== currentFile;
    });
    return filterFile;
};

var fn_routerFile = function(router,filterFile){
    filterFile.forEach(fileName =>{
        var fullFilePath = path.join(__dirname,fileName);
        var routerFile = require(fullFilePath);
    
        for(var fn in routerFile){
            var type = routerFile[fn][0];
            var fn_file = routerFile[fn][1];
            
    
            if(type === 'get'){
                router.get(fn,fn_file);
            }else if(type === 'post') {
                router.post(fn,fn_file);
            }
        }
    })
};

module.exports = function(){
    var filterFile = fn_filterFile();
    fn_routerFile(router,filterFile);
    return router.routes();
}