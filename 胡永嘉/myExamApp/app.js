'use strict'

const Koa = require('koa');
const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const controllers = require('./controllers');
const templating = require('./templating');
const koa_static = require('koa-static');

var app = new Koa();
app.use(bodyparser());
app.use(controllers());
app.use(templating);
app.use(koa_static(__dirname));

app.use(async (ctx,next)=>{
    ctx.render('hello.html',{
        hello:'你好！世界！！'
    })
})






var port = 5200;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);