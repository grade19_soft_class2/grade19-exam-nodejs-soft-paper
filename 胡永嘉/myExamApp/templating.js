'use strict'

const nunjucks = require('nunjucks');

var createEnv = function (path, opts) {
    var path = path || 'views';
    var opts = opts || {};

    var envOpts = {
        watch: opts.watch === undefined ? false : opts.watch
    }

    var env = nunjucks.configure(path,envOpts);
    return env;
};

module.exports = async (ctx,next)=>{
    ctx.render = function(view,modle){
        var env = createEnv('views');
        ctx.response.body = env.render(view,modle);
    };
    await next();
}