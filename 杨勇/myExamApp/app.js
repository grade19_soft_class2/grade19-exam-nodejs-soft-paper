'use strict'

let Koa=require('koa');

let staticRes=require('koa-static');

let templting=require('./templating')

let bodyParser=require('koa-bodyparser');

let contralls=require('./controllers');

let app=new Koa();

app.use(staticRes(__dirname+'/static'));

app.use(templting);

app.use(bodyParser());

app.use(contralls());

let port=3000;

app.listen(port)

console.log(`http://127.0.0.1:${port}`);

