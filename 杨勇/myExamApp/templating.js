'use strict'

let nunjucks=require('nunjucks')

function createEnv(path,opts){

     path=path||'view',

     opts=opts||{};

     let options={
         
     }

     let env=nunjucks.configure(path,options);

     return env;

}




module.exports=async (ctx,next)=>{
    ctx.render=function(views,model){
    
       let env=createEnv();

       ctx.body=env.render(views,model)

    }
    await next();
}