'use strict'

let fs=require('fs');

let path=require('path');

let router=require('koa-router')();

//筛选文件

function filesFilter(){

    let files=fs.readdirSync(__dirname);

    return files.filter(name=>{
        return name.endsWith('.js')&&name!=='index.js'
    })

}

//注册路由

function registerRouter(files){
    files.forEach(item=>{
        let newFiles=path.join(__dirname,item);

        let regnewFiles=require(newFiles);

        for(var x in regnewFiles){
            let type=regnewFiles[x][0];

            let fn=regnewFiles[x][1];

            if(type=='get'){
                router.get(x,fn)
            }

            else if(type=='post'){
                router.post(x,fn)
            }
        }
    })
}

module.exports=function(){
    registerRouter(filesFilter());

    return router.routes();
}