'use strict'

let login_fn=async (ctx,next)=>{

     ctx.render('login.html')

}

let loginDone=async (ctx,next)=>{
     let uid=ctx.request.body.username||'';
     let pwd=ctx.request.body.password||'';
     if(uid=='admin'&&pwd=='123'){
         ctx.redirect('/loginisok')
     }
     else{
         ctx.redirect('/loginerr')
     }
}

let loginerr=async (ctx,next)=>{
         ctx.body='登录失败'
}

let loginisok=async (ctx,next)=>{
         ctx.redirect('/hello')
}

let hello_fn=async (ctx,next)=>{
     ctx.render('hello.html')
}

module.exports={
    '/':['get',login_fn],
    '/loginDone':['post',loginDone],
    '/loginerr':['get',loginerr],
    '/loginisok':['get',loginisok],
    '/hello':['get',hello_fn]
}