'use strict'


let

    click_fn = async (ctx, next) => {
        ctx.body =
            `
            <h1>你现在在 / 下 请点击下方按钮去到 hello </h1>
        <form action="/hello" method="post">
            <input type="submit" value="请点击去到 hello">
        </form>
        
        `
    },
    hello_fn = async (ctx, next) => {
        ctx.render('hello.html')
    };



module.exports = {
    '/': ['get', click_fn],
    '/hello': ['post', hello_fn]
};