'use strict'

// 引入模块
let
    Koa = require('koa'),
    bodyParser = require('koa-bodyparser'),
    staticRes = require('koa-static'),
    templating = require('./templating'),
    controllers = require('./controllers');



let app = new Koa();

// 注册 bodyparser 中间件 用于处理 ctx.body 请求
app.use(bodyParser());
// 注册 static 中间件 用于处理 一些静态资源
app.use(staticRes(__dirname + '/static'));

// 注册 自定义模板引擎 用于渲染页面
app.use(templating);

// 注册 并 启动路由
app.use(controllers());

// 定义的端口号
let port = 3000;
// 设置web服务端的监听端口
app.listen(port, () => {
    console.log(`http://localhost:${port}`)
});