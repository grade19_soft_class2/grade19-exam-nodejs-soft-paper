'use strict'
let Koa=require('koa');
let app=new Koa();
let con=require('./controllers');
let sta=require('koa-static');
let tem=require('./templating')
app.use(sta(__dirname+'/static'))
app.use(con())
app.use(tem)
app.use(async(ctx,next)=>{
    ctx.render('hello.html');
})


let port=4000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);