'use strict'
let fs = require('fs');
let path = require('path');
let router = require('koa-router')();

function aaa() {
    let hq = fs.readdirSync(__dirname);
    let gl = hq.filter(name => {
        return name.endsWith('.js') && name != 'index.js';
    })
    return gl
}
function bbb(a) {
    a.forEach(item => {
        let hb = path.join(__dirname, item)
        let mk = require(hb);
        for (let i in mk) {
            let type = mk[i][0];
            let fn = mk[i][1];

            if(type==='get'){
                router.get(i,fn)
            }else if(type==='post'){
                router.post(i,fn)
            }
        }
    });
}
module.exports=function(){
    bbb(aaa())
    return router.routes();
}