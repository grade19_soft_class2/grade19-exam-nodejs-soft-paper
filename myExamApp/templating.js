'use strict'
let nunjucks=require('nunjucks');

function createEnv(path,opts){
    path=path||'view';
    opts=opts||'';
    let option={
        autoescape:opts.autoescape===undefined?true:opts.autoescape,
        throwOnUndefined:opts.throwOnUndefined===undefined?false:opts.throwOnUndefined,
        watch:opts.watch===undefined?true:opts.watch,
        noCache:opts.noCache===undefined?true:opts.noCache
    }
    let env=nunjucks.configure(path,option)
    return env;
}


module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
        let env=createEnv('view');
        ctx.body=env.render(view,model)
    }
    await next();
}
