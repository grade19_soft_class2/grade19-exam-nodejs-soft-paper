let Koa=require("koa");

let app=new Koa();
let bodyparser=require("koa-bodyparser");
let static=require("koa-static")
let templating=require("./templating")

let mod=require("./controllers/index")

app.use(bodyparser())
app.use(static(__dirname))
app.use(templating)

app.use(mod())
let port=3100;
app.listen(port)
console.log(`http://localhost:${port}`);