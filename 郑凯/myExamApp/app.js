'use strict';
let Koa = require('koa');
let Parser = require('koa-bodyparser');
let controllers = require('./controlllers')
let staticRes = require('koa-static');
let temp = require('./templating')
let port = 4080;
let app = new Koa();
app.use(Parser());
app.use(staticRes(__dirname));
app.use(temp);
app.use(controllers());



app.listen(port);
console.log(`http://localhost:${port}`);