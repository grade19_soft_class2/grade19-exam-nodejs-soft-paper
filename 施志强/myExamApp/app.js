'use strict'
let Koa=require('koa');
let bodyParser=require('koa-bodyparser');
let controllers=require('./controllers');
let templating=require('./templating');
let koa_static=require('koa-static');
let app=new Koa();

app.use(templating);
app.use(bodyParser());
app.use(koa_static(__dirname));
app.use(controllers());

app.use(async(ctx,next)=>{
    ctx.render('index.html',{nickname:'柑橘可以'})
})

let port=4578;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);