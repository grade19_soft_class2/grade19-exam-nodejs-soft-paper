'use strict'
let nunjucks=require('nunjucks');
function createEnv(path,opts){
    path=path || 'views',
    opts=opts || {};
    let envOpting={
        autoescape:opts.autoescape || true,
        throwOnUndefined:opts.throwOnUndefined || false,
        watch:opts.watch || false,
        onCache:opts.onCache || true
    }
    let env=nunjucks.configure(path,envOpting);
    return env;
}
module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
        let env=nunjucks.configure('views',{autoescape:false});
        ctx.body=env.render(view,model);
    }
    await next();
}