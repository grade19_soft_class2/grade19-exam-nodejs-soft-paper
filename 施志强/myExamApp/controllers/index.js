'use strict'
let fs=require('fs');
let path=require('path');
let router=require('koa-router')();
function getControll(dir){
    let filse=fs.readdirSync(dir);
    let controllFilse=filse.filter(name=>{
        return name.endsWith('.js') && name!=='index.js';
    })
    return controllFilse;
}
function regControll(filse){
    filse.forEach(item=>{
        let tmpFilse=path.join(__dirname,item);
        let tmpRoutes=require(tmpFilse);
        for(var i in tmpRoutes){
            let type=tmpRoutes[i][0];
            let fn=tmpRoutes[i][1];
            if(type==='get'){
                router.get(i,fn);
            }else if(type==='pose'){
                router.post(i,fn);
            }else if(type==='put'){
                router.put(i,fn);
            }else if(type==='delete'){
                router.delete(i,fn);
            }
        }
    })
}

module.exports=function(){
    let dafu=__dirname;
    let controllFilse=getControll(dafu);
    regControll(controllFilse);
    return router.routes();
}