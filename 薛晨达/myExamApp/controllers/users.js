'use strict';

let hello_fn = async (ctx, next) => {
    ctx.render('hello.html');
}

module.exports = {
    '/': ['get', hello_fn]
}