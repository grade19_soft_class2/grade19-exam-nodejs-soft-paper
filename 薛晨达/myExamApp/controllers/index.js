'use strict';

const fs = require('fs');
const path = require('path');
const router = require('koa-router')();

function Ctrls() {
    let files = fs.readdirSync(__dirname);
    let filterFile = files.filter(fileName => {
        return fileName.endsWith('.js') && fileName !== 'index.js';
    });
    return filterFile;
}

function registRouter(file) {
    file.forEach(item => {
        let fullPath = path.join(__dirname, item);
        let routerObj = require(fullPath);
        for (let item in routerObj) {
            let type = routerObj[item][0];
            let fn = routerObj[item][1];
            if (type === 'get') {
                router.get(item, fn);
            } else{
                router.post(item, fn);
            }
        }
    });
}

module.exports = function () {
    let file = Ctrls();
    registRouter(file);
    return router.routes();
}