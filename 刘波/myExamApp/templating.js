"use strict";

let nunjucks = require("nunjucks");
//创建配置env
function createEnv(path,opts){
    path = path ||"views";
    opts = opts || {};

    //配置
    let optionEnv = {
        autoescape:opts.autoescape===undefined? true:opts.autoescape,
        watch:opts.watch===undefined? false:opts.watch
    }

    let env = nunjucks.configure(path,optionEnv);

    return env;
}

module.exports=async (ctx,next)=>{
    //创建读取文件模块方法
    ctx.render = function(name,model){
        let env = createEnv();
        ctx.body = env.render(name,model);
    }
    await next();
}