'use strict';

let Koa = require('koa');
let port = 3000;

let staticRes = require("koa-static");
let bodyParser = require('koa-bodyparser');

let conIndex = require('./controllers');
let template = require('./templating');

let server = new Koa();

server.use(staticRes(__dirname + '/statics'))
server.use(bodyParser());
server.use(conIndex());
server.use(template);

server.use(async (ctx, next) => {
    ctx.render('hello.html', { name: '卤蛋卤蛋' });
});

server.listen(port);

console.log(`http://127.0.0.1:${port}`);