'use strict';

let products_list = async (ctx, next) => {
    ctx.body = '这里是产品列表';
};

let products_details = async (ctx, next) => {
    ctx.body = '这里是产品详情';
};

module.exports = {
    '/products/list': ['get', products_list],
    '/products/details': ['get', products_details]
}