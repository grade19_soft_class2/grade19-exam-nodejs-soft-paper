'use strict';

let home_fn = async (ctx, next) => {
    ctx.body = '<h1>登录成功，Welcome to Home Page!!</h1>';
};

module.exports = {
    '/home/home': ['get', home_fn]
};