'use strict';

//users get
let users_fn = async (ctx, next) => {
    ctx.body = `
    <form action="/users/login" method="post">
        <table>
            <div>
            <label for="">用户名：</label>    
            <input type="text" name="username">
            </div>
            <div>
                <label for="">密码：</label>
                <input type="password" name="password">
            </div>
            <div>
                <input type="submit" value="登录">
            </div>
        </table>
    </form>
    `;
};


//login post

let login_fn = async (ctx, next) => {
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';

    if (username === 'admin' && password === '123') {
        ctx.redirect('/home/home'); //登录成功，跳转到首页
    } else {
        ctx.body = '<h1>用户名或密码错误，请确认后重试!</h1>';
    }
};

module.exports = {
    '/': ['get', users_fn],
    '/users/login': ['post', login_fn]
};