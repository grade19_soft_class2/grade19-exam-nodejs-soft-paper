'use strict'

let nunjucks=require('nunjucks');

function createEnv(path,options){
    path=path||"views";
    options=options||{};

    let optionsLoad={
        autoescape:options.autoescape===undefined?true:options.autoescape,
        throwOnUndefined:options.throwOnUndefined===undefined?false:options.throwOnUndefined,
        watch:options.watch===undefined?true:options.watch,
        noCache:options.noCache===undefined?true:options.noCache
    };

    let env=nunjucks.configure(path,optionsLoad);

    return env;

};

module.exports=async(ctx,next)=>{
    ctx.render=function (views,model){
        let env=createEnv("views",{autoescape:true});
        ctx.body=env.render(views,model);
    }
    await next();
}