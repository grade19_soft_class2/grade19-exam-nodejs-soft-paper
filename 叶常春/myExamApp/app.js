'use strict';
let Koa = require('koa');
let bodyparser = require('koa-bodyparser');
let statics = require('koa-static');
let controllers = require('./controllers');
let templating = require('./templating');


let app = new Koa();
app.use(bodyparser());
app.use(templating);
app.use(statics(__dirname+'/static'));

app.use(controllers());



let port = 3000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);