let index_fn = async (ctx,next)=>{
    ctx.render('hello.html');
}

let login_fn = async (ctx,next)=>{
    ctx.render('login.html');
}

let register_fn = async (ctx,next)=>{
    ctx.render('register.html');
}

module.exports = {
    '/':['get',index_fn],
    '/login':['get',login_fn],
    '/register':['get',register_fn]
}