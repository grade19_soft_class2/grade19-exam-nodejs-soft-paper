const router =require('koa-router')();
const fs = require('fs');

function filterFile(){
   return fs.readdirSync(__dirname,'utf-8')
    .filter(item=> item.endsWith('.js') && item!=='index.js')
}


function registerRouter(file){
    file.forEach(item=>{
        let obj = require('./'+item)

        for (const key in obj) {
            let type = obj[key][0]

            if(type==='get'){
                router.get(key,obj[key][1])
            }else if(type==='post'){
                router.post(key,obj[key][1])
            }else if(type==='put'){
                router.put(key,obj[key][1])
            }else if(type==='delete'){
                router.delete(key,obj[key][1])
            }
        }
    })
}

module.exports =function(){
    let file = filterFile();
    registerRouter(file);
    return router.routes();
}