const nunjucks = require('nunjucks');

let env = nunjucks.configure('views',{
    autoescape:true,
    watch:true,
    noCache:true
})



module.exports=async (ctx,next)=>{
    ctx.render=(view,data)=>{
        ctx.body = env.render(view,data)
    }

    await next();
}