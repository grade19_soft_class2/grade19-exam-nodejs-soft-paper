'use strict'

let fn_1 = async(ctx,next)=>{
    ctx.body='1'
}

let fn_2 = async(ctx,next)=>{
    ctx.body='2'
}

let fn_3 = async(ctx,next)=>{
    ctx.body='3'
}

let fn_4 = async(ctx,next)=>{
    ctx.body='4'
}

module.exports={
    '/fn11':['get',fn_1],
    '/fn22':['get',fn_2],
    '/fn33':['get',fn_3],
    '/fn44':['get',fn_4]
}