'use strict';
 
let fs=require("fs");
let path=require("path");
let router=require("koa-router")();

function filterFile(){
    let file=fs.readdirSync(__dirname);
    let filter=file.filter(f=>{
        return f.endsWith('.js')&&f!=='index.js';
    });
    return filter;
}


function register(files){
   files.forEach(element => {
       let fullPath=path.join(__dirname,element);
       let routerFile=require(fullPath);
       for(let i in routerFile){
           let type=routerFile[i][0];
           let fn=routerFile[i][1];
           if(type==='post'){
               router.post(i,fn);
           }else if(type==='get'){
               router.get(i,fn);
           }
       }
   });
}

module.exports=function(){
    let controller=filterFile();
    register(controller);
    return router.routes();
}