'use strict';
let login_fn=async(ctx,next)=>{
    ctx.render('hello.html');
}


let login_Done=async(ctx,next)=>{
    let username=ctx.request.body.username||"";
    let password=ctx.request.body.password||"";

    if(username==="admin"&&password==="123"){
      ctx.redirect('/success');
    }else{
        ctx.redirect('/defeat');
    }
}

let login_success=async(ctx,next)=>{
    ctx.body="登录成功";
}
let login_defeat=async(ctx,next)=>{
    ctx.body="登录失败"
}

module.exports={
  
    '/':["get",login_fn],
    '/loginDone':["post",login_Done],
    '/success':["get",login_success],
    '/defeat':["get",login_defeat]
}