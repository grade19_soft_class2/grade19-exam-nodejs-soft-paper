'use strict';
let nunjucks=require('nunjucks');

function createEnv(path,opts){
    path=path||'views';
    opts=opts||{};
    let envOptions={
        autoescape:opts.autoescape===undefined?true:opts.autoescape,
        throwOnUndefined:opts.throwOnUndefined?false:opts.throwOnUndefined,
        trimBlocks:opts.trimBlocks?false:opts.trimBlocks,
        isTrimBlocks:opts.isTrimBlocks?false:opts.isTrimBlocks,
        watch:opts.watch?false:opts.watch,
        noCache:opts.noCache?false:opts.noCache
    }
    let env=nunjucks.configure(path,envOptions);
    return env;
}

module.exports=async(ctx,next)=>{
    ctx.render=function(path,model){
     let env=createEnv('views',{watch:true,noCache:true,autoescape:false});
     ctx.body=env.render(path,model);
    }
    await next();
}