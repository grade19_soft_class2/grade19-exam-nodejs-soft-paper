'use strict';
let Koa=require("koa");
let statics=require("koa-static");
let bodyParser=require("koa-bodyparser");
let templating=require('./templating');
let controller=require('./controllers');



let app=new Koa();
app.use(statics(__dirname+'/static'));
app.use(bodyParser());
app.use(templating);
app.use(controller());

let port=3000;
app.listen(port,()=>{
    console.log(`http://localhost:${port}`);
});
