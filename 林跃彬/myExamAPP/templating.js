'use strict';
let nunjucks = require('nunjucks');

function createEnv(path,opts){
    path = path || 'views';
    opts = opts || {};

    let confOpts = {
        autoescape : opts.autoescape || true,
        throwOnUndefined : opts.throwOnUndefined || false,
        
    }

    let env =nunjucks.configure(path,confOpts);
    return env;
}

module.exports = async (ctx,next)=>{
    let env = createEnv();
    ctx.render = function (views,model){
        ctx.body = env.render(views,model);
    }
    await next();
}