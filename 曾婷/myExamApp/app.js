'use strict';

//导入模块
let Koa = require('koa');
let bodyParser = require('koa-bodyparser');
let stati = require('koa-static');
let con = require('./controllers');
let temp = require('./templating');

//初始化server实例
let server = new Koa();

//注册静态资源处理
server.use(stati(__dirname));

//注册使用中间件
server.use(bodyParser());
server.use(temp);
server.use(con());

//监听端口
let port = 3000;
server.listen(port,()=>{
    console.log(`http://127.0.0.1:${port}`);
})