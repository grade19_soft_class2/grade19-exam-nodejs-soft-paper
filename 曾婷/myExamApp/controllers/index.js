'use strict';

//导入模块
let fs = require('fs');
let path = require('path');
let router = require('koa-router')();

//筛选此文件夹下以.js结尾的文件但不是index.js的文件
function filterControllers(){
    //获取文件路径
    let files = fs.readdirSync(__dirname);
    //使用过滤器筛选以.js结尾的文件但不是index.js的文件
    let filterFiles = files.filter(name => {
        return name.endsWith('.js') && name !== 'index.js';
    })
    return filterFiles;
}

//注册路由
function registerRouter(file){
    //遍历file
    file.forEach(x => {
        //拼接路径，获取完整路径
        let fullpath = path.join(__dirname,x);
        //获取路由
        let obj = require(fullpath);
        //注册路由
        for(let r in obj){
            let type = obj[r][0];
            let fn = obj[r][1];
            if(type === 'get'){
                router.get(r,fn);
            }
            else{
                router.post(r,fn);
            }
        }
    });
}

//作为中间件暴露出去，必须为一个函数
module.exports = function(){
    //调用函数filterControllers（）
    let obj = filterControllers();
    //调用函数registerRouter（）
    registerRouter(obj);
    //返回路由
    return router.routes();
}