'use strict'

//首页
let hello_fn = async (ctx,next)=>{
    ctx.render('hello.html');
}

//暴露
module.exports = {
    '/':['get',hello_fn]
}