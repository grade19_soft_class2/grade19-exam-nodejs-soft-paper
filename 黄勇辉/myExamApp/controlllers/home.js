'use strict'
let index_fn = async (ctx,next)=>{
    ctx.render('index.html');
};


let login_fn = async (ctx,next)=>{
    ctx.body = `<h1>这里登录</h1>`
};

let register_fn = async (ctx,next)=>{
    ctx.body = '<h1>这里注册</h1>'
};




module.exports = {
    '/':['get',index_fn],
    '/login':['get',login_fn],
    '/register':['get',register_fn],
}