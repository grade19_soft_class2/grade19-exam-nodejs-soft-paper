'use strict'
const fs = require('fs');
const path = require('path');
const router = require('koa-router')();

function serchfiles() {   
    let files = fs.readdirSync(__dirname);
    let resultFiles = files.filter(fileName => {
        return fileName.endsWith('.js') && fileName !== 'index.js';
    });
  
    return resultFiles;
}
function fun(files) {
    files.forEach(item => {       
        let fullPath = path.join(__dirname, item);    
        let routerObj = require(fullPath);    
        for (let item in routerObj) {       
            let type = routerObj[item][0];           
            let fn = routerObj[item][1];
            if (type === 'get') {
                router.get(item, fn);
            } else {
                router.post(item, fn);
            }
        }
    });
}
module.exports = function () {
    let files = serchfiles();
    fun(files);
    return router.routes();
}