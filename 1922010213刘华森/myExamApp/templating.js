'use strict';
let nunjucks=require('nunjucks');

function createEnv(path, opts) {
    path = path || 'views',
    opts = opts || {};
    let envOptions = {
        autoescape: opts.autoescape === undefined ? true : opts.autoescape,
        throwOnUndefined: opts.throwOnUndefined === undefined ? false : opts.throwOnUnderfined,
        watch: opts.watch === undefined ? false : opts.watch,
        noCache: opts.noCache === undefined ? false: opts.noCache
    }
    let env = nunjucks.configure(path, envOptions)
    return env;
}
module.exports = async (ctx, next) => {
    ctx.render = function (view, model) {
        let env = createEnv('views', { autoescape: false });
        ctx.body = env.render(view, model);
    }
    await next();
}
