'use strtic';
let fs=require('fs');
let path=require('path');
let router=require('koa-router')();

function getFilesName(){
    let files=fs.readdirSync(__dirname);
    let getfiles=files.filter((name)=>{
        return name.endsWith('.js') && name!='index.js'
    })
    return getfiles;
}
function getForEach(filess){
    filess.forEach(item=>{
        let parhfile=path.join(__dirname,item);
        let temFile=require(parhfile);
        for(var x in temFile){
            let type=temFile[x][0];
            let fn=temFile[x][1];
            if (type === 'get') {
                router.get(x, fn);
            }
            else if (type === 'post') {
                router.post(x, fn);
            }
        }
    })
}
module.exports=function(){
    let are=getFilesName();
    getForEach(are);
    return router.routes();
}




