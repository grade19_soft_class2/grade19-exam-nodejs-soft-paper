'use strict'
let Koa=require('koa');

let bodyparser=require('koa-bodyparser');
let templating=require('./templating');
let controllers=require('./controllers');
let statices=require('koa-static');


let app=new Koa();
app.use(bodyparser());
app.use(templating);
app.use(statices(__dirname+'/static'));
app.use(controllers());

let port=3032;
app.listen(port);
console.log(`IP:http://127.0.0.1:${port}`);