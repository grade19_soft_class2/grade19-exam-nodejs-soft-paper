var fs = require('fs');
var path = require('path');
var router = require('koa-router')();



function getEachControllerFiles(d){
    var file = fs.readdirSync(d);

    var confile = file.filter((name) => {
        return name.endsWith('.js') && name !== 'index.js';
    });
    return confile;
}

function registeRoutes(confile){
    confile.forEach(x => {
        var temPath = path.join(__dirname, x);
        var tmpRoutes = require(temPath);
        for (var v in tmpRoutes) {
            var type = tmpRoutes[v][0];
            var fs = tmpRoutes[v][1];
            if (type === 'get') {
                router.get(v, fs);
            } else  {
                router.post(v, fs)
            }
        }
    })
}

module.exports = function () {
    var workPath = __dirname;
    var controllerFiles=getEachControllerFiles(workPath);
    registeRoutes(controllerFiles);
    return router.routes()
}