let index_fn=async(ctx,next)=>{
 ctx.render('index.html')
}
let login_fn=async(ctx,next)=>{
    ctx.render('login.html')
}
let hello_fn=async(ctx,next)=>{
    ctx.render('hello.html')
}
let loginDone_fn=async(ctx,next)=>{
    let uid=ctx.request.body.username;
    let pwd=ctx.request.body.password;
    if(uid==='admin'&& pwd==='123'){
        ctx.redirect('/index')
    }else{
        ctx.redirect('/hello')
    }
}

module.exports={
    '/':['get',login_fn],
    '/index':['get',index_fn],
    '/hello':['get',hello_fn],
    '/loginDone':['post',loginDone_fn],
}