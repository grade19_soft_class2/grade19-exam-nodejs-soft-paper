'use strict';

let Koa = require('koa');
let bodyparser = require('koa-bodyparser');
let constrollers = require('./controllers');
let tamplating = require('./templating');
let koa_static = require('koa-static');

var app = new Koa();

app.use(tamplating);
app.use(koa_static(__dirname));
app.use(bodyparser());
app.use(constrollers());


app.use(async (ctx, next) => {
    ctx.render('hello.html', {
        nameone: '六花色',
        nametwo: '胡小胖'
    })
})

app.listen(5566)
console.log('http://127.0.0.1:5566');
