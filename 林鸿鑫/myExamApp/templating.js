'use strict'
const nunjucks = require('nunjucks');


var createEev = function (path, opts) {
    path = path || 'views';
    opts = opts || {};

    var opts = {
        autoescape: opts.autoescape === undefined ? true : opts.autoescape,
        throwUndefined: opts.throwUndefined === undefined ? false : opts.throwUndefined,
        trimBlocks: opts.trimBlocks === undefined ? false : opts.trimBlocks,
        lstripBlocks: opts.lstripBlocks === undefined ? false : opts.lstripBlocks,
        watch: opts.watch === undefined ? false : opts.watch,
        noCache: opts.noCache == -undefined ? false : opts.noCache
    };
    var env = nunjucks.configure(path, opts);
    return env;
}
module.exports = async (ctx, next) => {
    ctx.render = function (view, model) {
        var env = createEev('views');
        ctx.response.body = env.render(view, model)
    };
    await next();
};