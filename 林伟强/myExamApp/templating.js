'use strict';

let nunjucks = require('nunjucks');

function createEnv(path,opts){
    path = path || 'views';
    opts = {};
    let envOption = {
        autoescpae:opts.autoescpae || true,
        throwOnUndefined:opts.throwOnUndefined || false,
        lstripBlock:opts.lstripBlock || false,
        trimBlock:opts.trimBlock || false,
        watch:opts.watch || false,
        noCache:opts.noCache || false
    }
    let env = nunjucks.configure(path,envOption);
    return env;

}

module.exports = async (ctx,next) =>{
    ctx.render=(view,model) =>{
        let env = createEnv();
        ctx.body = env.render(view,model);
    }
    await next();
}