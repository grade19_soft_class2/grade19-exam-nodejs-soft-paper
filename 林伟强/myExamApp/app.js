'use strict';

let Koa = require('koa');
let bodyparser = require('koa-bodyparser');
let templating = require('./templating');
let controller = require('./controllers');
let staticRes = require('koa-static');

let app = new Koa();

app.use(bodyparser());
app.use(templating);
app.use(controller());
app.use(staticRes(__dirname+'/static'));

app.use(async (ctx,next) =>{
    ctx.render('index.html' ,{name: '林宝欢迎您的光临' , title: '如果您有话想说，来这个网页留言: linbao.bachelorofspirit.xyz' });
});

let port = 3000;
app.listen(port);

console.log(`http://127.0.0.1:${port}`);