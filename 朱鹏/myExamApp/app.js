'use starct';



let Koa=require('koa');
let sta=require('koa-static');
let temp=require('./templating');
let con=require('./controllers');



let app=new Koa();

app.use(temp);
app.use(con());
app.use(sta(__dirname+'/statics'));



let port = 4650;
app.listen(port);

console.log(`http://127.0.0.1:${port}`);

