'use strict'

let nunjucks = require("nunjucks");

function creatEnv(path, opts) {
    path = path || 'views';
    opts = opts || {};

    let envO = {

    }

    let env=nunjucks.configure(path,envO)
    return env;
}



module.exports=async(ctx,next)=>{
    let env=creatEnv();
        ctx.render = function (views, model) {
        ctx.body = env.render(views, model);
    }
    await next();
}
