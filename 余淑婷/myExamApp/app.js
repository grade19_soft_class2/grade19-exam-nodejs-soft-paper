'use strict'

let koa = require('koa');
let bodyparser = require('koa-bodyparser');
let tem = require('./templating')
let con = require('./controllers/index')
let koa_static = require('koa-static')

let app =new koa()
app.use(bodyparser())
app.use(tem)
app.use(con())
app.use(koa_static(__dirname))


app.use(async function(ctx,next){
    ctx.render('hello.html')
})

let port = 3888;
app.listen(port);

console.log(`http://127.0.0.1:${port}`);