'use strict';

const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const statics = require('koa-static');
const temp = require('./templating');
const controllers = require('./controllers');


let app = new Koa();
app.use(statics(__dirname));
app.use(bodyparser());
app.use(temp);
app.use(controllers());

let port = 5000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);