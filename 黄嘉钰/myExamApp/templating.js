'use strict';

const nunjucks = require('nunjucks');

function createEnv(path,opts){
    path = path || 'views',
    opts = opts || {}

    let envOption = {
        watch : opts.watch === undefined ? true : opts.watch,
        noCache : opts.noCache === undefined ? true : opts.noCache,
        throwOnUndefined : opts.throwOnUndefined === undefined ? true : opts.throwOnUndefined,
        autoescape : opts.autoescape === undefined ? true : opts.autoescape
    }

    let env = nunjucks.configure(path,envOption);

    return env;
}

module.exports = async (ctx,next)=>{
    ctx.render = function(view,model){
        let env = createEnv();
        ctx.response.body = env.render(view,model);
    }
    await next();
}