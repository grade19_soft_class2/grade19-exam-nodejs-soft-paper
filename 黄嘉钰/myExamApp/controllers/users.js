'use strict';

let login_fn = async (ctx,next)=>{
    ctx.render('login.html');
}

let judge_fn = async (ctx,next)=>{
    let uid = ctx.request.body.username ||'';
    let pwd = ctx.request.body.password ||'';

    if(uid === 'root' && pwd === '111'){
        ctx.body = '成功';
    }else{
        ctx.body = '错误';
    }
}

module.exports = {
    '/' : ['get',login_fn],
    '/judge':['post',judge_fn]
}