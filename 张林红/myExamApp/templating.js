'use strict';

let nunjucks=require('nunjucks');

function createEnv(path,opts){
    path||'views';
    opts||{};

    let envOption={
        autoescape:opts.autoescape===undefined?true:opts.autoescape,
        throwOnUndefined:opts.throwOnUndefined===undefined?false:opts.throwOnUndefined,
        trimBlocks:opts.trimBlocks===undefined?false:opts.trimBlocks,
        IstripBlocks:opts.trimBlocks===undefined?false:opts.IstripBlocks,
        watch:opts.watch===undefined?true:opts.watch,
        onCache:opts.onCache==undefined?true:opts.onCache
    }

    let env=nunjucks.configure(path,envOption);
    return env;
} 

module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
        let env=createEnv('views',{autoescape:false})
        ctx.body=env.render(view,model)
    }
    await next();
}