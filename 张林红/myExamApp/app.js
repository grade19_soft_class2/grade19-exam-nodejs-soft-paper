'use strict';

let Koa=require('koa');
let koa_static=require('koa-static');
let bodyParser=require('koa-bodyparser');
let temp=require('./templating');
let controllers=require('./controllers');


let app=new Koa();
app.use(koa_static(__dirname+'/static'));
app.use(bodyParser());
app.use(temp);
app.use(controllers());

let port=3000
app.listen(port);
console.log(`http://127.0.0.1:${port}`);