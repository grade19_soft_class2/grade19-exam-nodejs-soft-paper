'use strict';

let hello_fn=async(ctx,next)=>{
    ctx.body='<h1>包装首页<h1>';
};
let list_fn =async(ctx,next)=>{
}
let add_fn =async(ctx,next)=>{
}
let edit_fn =async(ctx,next)=>{
}
let delete_fn =async(ctx,next)=>{
}
let details_fn =async(ctx,next)=>{
}


module.exports={
    '/':['get',hello_fn],
    '/user/list':['get',list_fn],
    '/user/add':['get',add_fn],
    '/user/edit':['get',edit_fn],
    '/user/delete':['get',delete_fn],
    '/user/details':['get',details_fn]
}