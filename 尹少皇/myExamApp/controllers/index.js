'use strict';
let fs=require('fs');
let path=require('path');
let router=require('koa-router')();

//获取文件路径
function getFiles(){
    let files=fs.readdirSync(__dirname);
    return files.filter(item=>{
        return item.endsWith('.js') && item !=='index.js'
    })
};

//获取路由
function getRouter(files){
    files.forEach(element => {
        let temPath=path.join(__dirname,element);
        let tempRouter=require(temPath);
        for(let i in tempRouter ){
            let type=tempRouter[i][0];
            let fn=tempRouter[i][1];
            if(type ==='get'){
                router.get(i,fn);
            }else if(type==='post'){
                router.post(i,fn);
            }
        }
    });
}

module.exports=function (){
    let files=getFiles();
    getRouter(files);
    return router.routes()
};