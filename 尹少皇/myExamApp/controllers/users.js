'use strict';
let default_fn = async (ctx, next) => {
    ctx.render('hello.html', { content: '<script>alert("(｡･∀･)ﾉﾞ嗨")</script>' })
};
let login_fn = async (ctx, next) => {
    ctx.body = `<form action="/loginDone" method="POST">
    <label for="">用户名：</label><input type="text" id="username" name="username">
    <label for="">密码：</label><input type="text" id="password" name="password">

    <input type="submit" value="登录" >
</form>`
}
let loginDone_fn = async (ctx, next) => {
    let uid = ctx.request.body.username || ''
    let pwd = ctx.request.body.password || ''
    console.log(uid);
    console.log(pwd);
    if (uid === 'admin' && pwd === '123') {
        ctx.redirect('/index')
    } else {
        ctx.body = '登录失败'
    }
}
module.exports = {
    '/': ['get', login_fn],
    '/index': ['get', default_fn],
    '/loginDone': ['post', loginDone_fn]
}