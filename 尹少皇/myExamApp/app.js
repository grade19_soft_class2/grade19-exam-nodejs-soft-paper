let Koa=require('koa');
let app=new Koa();
let bodyParser=require('koa-bodyparser');
let controll=require('./controllers');
let temp=require('./templating');
let server=require('koa-static')

//引入静态模块
app.use(server(__dirname))

//模板引擎，渲染
app.use(temp);

//注册中间件post
app.use(bodyParser());
//注册路由
app.use(controll());



let port=3000;
app.listen(port,()=>{
    console.log(`http://127.0.0.1:${port}`);
});