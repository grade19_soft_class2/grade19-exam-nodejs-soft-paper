'use strict';
let nunjucks = require('nunjucks');

function createEnv(path, optss) {
    path = path || 'views',
    optss = optss || {};
    let envoptssions = {
        autoescape: optss.autoescape === undefined ? true : optss.autoescape,
        throwOnUndefined: optss.throwOnUndefined === undefined ? false : optss.throwOnUnderfined,
        watch: optss.watch === undefined ? false : optss.watch,
        noCache: optss.noCache === undefined ? false : optss.noCache
    }
    let env = nunjucks.configure(path, envoptssions)
    return env;
}
module.exports = async (ctx, next) => {
    ctx.render = function (view, model) {
        let env = createEnv('views', { autoescape: false });
        ctx.body = env.render(view, model);
    }
    await next();
}
