'use strtic';


let Koa=require("koa");
let bodyparser=require("koa-bodyparser");
let controller=require('./controllers');
let tem=require("./templating");
let static=require("koa-static");



let app=new Koa();
app.use(static(__dirname));
app.use(bodyparser());
app.use(tem);
app.use(controller());

let port=5320;
app.listen(port,()=>{
    console.log(`http://127.0.0.1:${port}`);
})
