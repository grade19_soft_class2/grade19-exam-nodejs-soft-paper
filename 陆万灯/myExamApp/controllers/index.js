'use strtic';

let fs = require('fs');
let path = require("path");
let router = require('koa-router')();

function returnFilename() {
    let getfileName = fs.readdirSync(__dirname);
    let getFilterNileName = getfileName.filter((name) => {
        return name.endsWith('.js') && name != 'index.js'
    })
    return getFilterNileName
};

function getTraversalFile(filess) {
    filess.forEach(item => {
        let filePathName = path.join(__dirname, item);
        let file = require(filePathName);
        for (var x in file) {
            let type = file[x][0];
            let fn = file[x][1];
            if (type === 'get') {
                router.get(x, fn);
            }
            else if (type === 'post') {
                router.post(x, fn);
            }
            else if (type === 'put') {
                router.put(x, fn);
            }
            else if (type === 'delete') {
                router.delete(x, fn);
            }
        }
    })
}
module.exports = function () {
    let aa = returnFilename();
    getTraversalFile(aa);
    return router.routes();
}