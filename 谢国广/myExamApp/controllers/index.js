const router =require('koa-router')();
const fs = require('fs')



function filterFile(){
   let file= fs.readdirSync(__dirname,'utf-8')
    .filter(item=> item.endsWith('.js') && item!=='index.js')
    console.log(file);
    return file
}

function registerRouter(file){
    
    file.forEach(item=>{
        let obj =require('./' + item);
        for (const key in obj) {
            let value = obj[key]
            console.log(value);

            if(value[0]==='get'){
                router.get(key,value[1])
            }else if(value[0]==='post'){
                router.post(key,value[1])
            }else if(value[0]==='delete'){
                router.delete(key,value[1])
            }else if(value[0]==='put'){
                router.put(key,value[1])
            }
        }
    })
}

module.exports =function(){
    let file =filterFile();
    registerRouter(file);

    return router.routes();
}

