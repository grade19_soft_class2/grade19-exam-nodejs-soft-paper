const Koa =require('koa');
const app = new Koa();
const bodyparser = require('koa-bodyparser')
const static =require('koa-static')
const templating = require('./templating')
const routes =require('./controllers')

app.use(static('static'))
app.use(bodyparser())
app.use(templating)
app.use(routes())


app.listen(5200)
console.log('http://localhost:5200');
