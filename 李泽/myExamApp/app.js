'use strict';


const Koa = require('koa');


const statics = require('koa-static');


const temp = require('./templating');


const router = require('./controllers');


const bodyParser = require('koa-bodyparser');


const server = new Koa();


server.use(statics(__dirname + '/static'));


server.use(bodyParser());


server.use(temp);


server.use(router());


let port = 3000;
server.listen(port, () => {
    console.log(`http://127.0.0.1:${port}`);
});