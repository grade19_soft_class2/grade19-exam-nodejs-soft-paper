`use strict`

let Koa=require('koa');
let bodyparser=require('koa-bodyparser');
let contrullers=require('./contrullers');
let tem=require('./templeting');
let statics=require('koa-static');

let app=new Koa();

app.use(bodyparser());
app.use(tem);
app.use(statics(__dirname+'/static'));
app.use(contrullers());

let port=3000;
app.listen(port);

console.log(`http:127.0.0.1:${port}`);