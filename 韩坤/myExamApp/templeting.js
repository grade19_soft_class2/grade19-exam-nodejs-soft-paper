let nunjucks=require('nunjucks');

function createEnv(path,opts){
    path=path || 'views';
    opts=opts || {};

    envOpts={
        autoescape:opts.autoescape || true,
        throwOnUndind:opts.throwOnUndind || false,
        trimBlocks:opts.trimBlocks || false,
        lstripBlocks:opts.lstripBlocks || false,
        watch:opts.watch || true,
        noCache:opts.noCache || true
    }

    let env=nunjucks.configure(path,envOpts);
    return env;
}

module.exports=async(ctx,next)=>{
    let env=createEnv();
    ctx.render=function(views,model){
        ctx.body=env.render(views,model);
    }

    await next();
};