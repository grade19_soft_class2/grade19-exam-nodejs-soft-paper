'use strict'

let fs = require('fs');
let router = require('koa-router')();
let path = require('path');

function findFile(path) {
    let files = fs.readdirSync(path)
    return files.filter(fileName => {
        return fileName.endsWith('.js') && fileName !== 'index.js'
    })
}

function fun(files) {
    files.forEach(item => {
        let filePath = path.join(dir, item);
        let routerobj = require(filePath);
        for (let x in routerobj) {
            let type = routerobj[x][0];
            let fn=routerobj[x][1];

            if (type === 'get') {
                router.get(type, fn);
            } else {
                router.post(type, fn);
            }
        }
    });
}

